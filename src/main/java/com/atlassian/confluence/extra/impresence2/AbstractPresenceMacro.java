/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.extra.impresence2.reporter.PresenceException;
import com.atlassian.confluence.extra.impresence2.reporter.PresenceReporter;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * A simple base class for our presence macros.
 */
public abstract class AbstractPresenceMacro extends LocaleAwareMacro implements Macro
{
    private static final Logger logger = LoggerFactory.getLogger(AbstractPresenceMacro.class);

    private final PresenceManager presenceManager;

    private final PermissionManager permissionManager;

    private final VelocityHelperService velocityHelperService;

    protected AbstractPresenceMacro(LocaleManager localeManager, I18NBeanFactory i18NBeanFactory, PresenceManager presenceManager, PermissionManager permissionManager, VelocityHelperService velocityHelperService)
    {
        super(localeManager, i18NBeanFactory);
        this.presenceManager = presenceManager;
        this.permissionManager = permissionManager;
        this.velocityHelperService = velocityHelperService;
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        String service = getImService(parameters);
        if (service == null)
            throw new MacroExecutionException(getText("error.macro.noserviceidprovided"));

        PresenceReporter reporter = getReporter(service);
        if (reporter == null)
            throw new MacroExecutionException(getText("error.macro.unsupportedservice", new Object[]{service}));

        if (reporter.requiresConfig())
        {
            return getRenderedHtml(reporter);
        }
        else
        {
            try
            {
                return reporter.getPresenceXHTML(GeneralUtil.htmlEncode(getImId(parameters)), shouldShowId(parameters));
            }
            catch (IOException e)
            {
                logger.error("IO error while getting presence.", e);
                throw new MacroExecutionException(e);
            }
            catch (PresenceException e)
            {
                logger.error("Error getting presence", e);
                throw new MacroExecutionException(e);
            }
        }
    }

    public String execute(Map parameters, String bodyText, RenderContext renderContext) throws MacroException
    {
        try
        {
            return execute(parameters, bodyText, new DefaultConversionContext(renderContext));
        }
        catch (MacroExecutionException macroError)
        {
            throw new MacroException(macroError);
        }
    }

    protected String getRenderedHtml(final PresenceReporter reporter)
    {
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();

        velocityContext.put("remoteUser", AuthenticatedUserThreadLocal.getUser());
        velocityContext.put("reporter", reporter);
        velocityContext.put("isGlobalAdmin", permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM));

        return velocityHelperService.getRenderedTemplate("templates/extra/impresence2/reporter-not-configured.vm", velocityContext);
    }

    protected PresenceReporter getReporter(final String service)
    {
        return presenceManager.getReporter(service);
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context)
    {
        return TokenType.INLINE;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public OutputType getOutputType()
    {
        return OutputType.INLINE;
    }

    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    /**
     * Gets the user ID to show presence of. Sub-classes may override this method to provide the necessary transformation
     * on the specified ID.
     *
     * @param  parameters
     * The macro parameters map containing the user ID
     *
     * @return
     * The user ID to show the presence of
     */
    protected String getImId(Map<String, String> parameters)
    {
        return parameters.get("0");
    }

    protected abstract String getImService(Map<String, String> parameters);

    /**
     * Checks whether the user ID should be displayed along with the presence information
     *
     * @param  parameters
     * The macro parameters map containing the user ID
     * 
     * @return
     * Returns {@code true} if the user ID should be shown; {@code false} otherwise
     */
    protected boolean shouldShowId(Map<String, String> parameters)
    {
        return !StringUtils.equalsIgnoreCase(parameters.get("showid"), Boolean.FALSE.toString());
    }
}
