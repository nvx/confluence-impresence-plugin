/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.renderer.v2.RenderUtils;
import net.sf.jml.Email;
import net.sf.jml.MsnContact;
import net.sf.jml.MsnMessenger;
import net.sf.jml.MsnUserStatus;
import net.sf.jml.MsnSwitchboard;
import net.sf.jml.message.MsnInstantMessage;
import net.sf.jml.message.MsnControlMessage;
import net.sf.jml.message.MsnSystemMessage;
import net.sf.jml.message.MsnDatacastMessage;
import net.sf.jml.message.MsnUnknownMessage;
import net.sf.jml.event.MsnMessageAdapter;
import net.sf.jml.event.MsnMessengerAdapter;
import net.sf.jml.impl.MsnMessengerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;
import java.util.HashMap;

/**
 * Connects with and reports on the status of an MSN user.
 */
public class MSNPresenceReporter extends LoginPresenceReporter
{
    public static final String KEY = "msn";
    
    private static final Logger log = LoggerFactory.getLogger(MSNPresenceReporter.class);

    private static final String ONLINE = "NLN";
    private static final String AWAY = "AWY";
    private static final String BUSY = "BSY";
    private static final String BRB = "BRB";
    private static final String PHONE = "PHN";
    private static final String LUNCH = "LUN";
    private static final String OFFLINE = "FLN";

    private static final Map<String, String> STATUS_MAP = Collections.unmodifiableMap(
            new HashMap<String, String>()
            {
                {
                    put(ONLINE, "im_available");
                    put(AWAY, "im_away");
                    put(BUSY, "im_dnd");
                    put(BRB, "im_away");
                    put(PHONE, "im_away");
                    put(LUNCH, "im_away");
                    put(OFFLINE, "im_invisible");
                }
            }
    );

    private MsnMessenger messenger;
    private boolean loginComplete = false;

    protected String getPresenceURL(String id)
    {
        return "mailto:" + id;
    }

    ///// Standard methods /////

    public String getKey()
    {
        return KEY;
    }

    public String getName()
    {
        return getText("presencereporter.msn.name");
    }

    public String getServiceHomepage()
    {
        return getText("presencereporter.msn.servicehomepage");
    }

    /*
     * Made this method synchronized so that the <code>MsnMessengerListener</code> added to <code>messenger</code>
     * can't set <code>messenger</code> to <code>null</code> in the middle of the execution of this method and
     * cause <a href="http://developer.atlassian.com/browse/PRES-9">PRES-9</a>
     */
    public synchronized String getPresenceXHTML(String id, boolean outputId) throws PresenceException
    {
        if (messenger == null)
        {
            if (requiresConfig())
            {
                throw new PresenceException(getText("presencereporter.msn.error.needconfig"));
            }

            messengerLogin();
        }

        if (isLoginComplete() && null != messenger)
        {
            Email email = Email.parseStr(id);
            MsnContact contact = messenger.getContactList().getContactByEmail(email);

            if (contact == null)
            {
                messenger.addFriend(email, id);
                return RenderUtils.error(
                            getText("presencereporter.msn.message.waitinbuddyaccept"
                                    , new Object[] { id } ));
            }
            else
            {
                MsnUserStatus status = contact.getStatus();

                return getPresenceLink(id, getStatusImage(status.getStatus()), status.getDisplayStatus(), outputId);
            }
        }
        else
            return getPresenceLink(id, "im_invisible", getText("presence.link.loggingin"), outputId);
    }

    protected boolean isLoginComplete() 
    {
        return loginComplete;
    }

    private void messengerLogin()
    {
        loginComplete = false;
        messenger = createMsnMessenger();
        messenger.addMessengerListener(new MsnMessengerAdapter()
        {

            public void loginCompleted(MsnMessenger messenger)
            {
                synchronized (MSNPresenceReporter.this)
                {
                    loginComplete = true;
                }
            }

            public void logout(MsnMessenger msg)
            {
                synchronized (MSNPresenceReporter.this)
                {
                    messenger = null;
                    loginComplete = false;
                }
            }

            public void exceptionCaught(MsnMessenger messenger, Throwable throwable)
            {
                log.error("Exception caught from MSN Messenger", throwable);
            }
        });

        messenger.addMessageListener(new MsnMessageAdapter()
        {

            public void instantMessageReceived(MsnSwitchboard switchboard, MsnInstantMessage message, MsnContact contact)
            {
                defaultResponse(switchboard);
            }

            public void controlMessageReceived(MsnSwitchboard switchboard, MsnControlMessage message, MsnContact contact)
            {
                //defaultResponse(switchboard);
            }

            public void systemMessageReceived(MsnMessenger messenger, MsnSystemMessage message)
            {
                // Do Nothing
            }

            public void datacastMessageReceived(MsnSwitchboard switchboard, MsnDatacastMessage message, MsnContact contact)
            {
                //defaultResponse(switchboard);
            }

            public void unknownMessageReceived(MsnSwitchboard switchboard, MsnUnknownMessage message, MsnContact contact)
            {
                //defaultResponse(switchboard);
            }
        });

        messenger.login();

        try
        {
            // Wait for a second to let messenger log on.
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    protected MsnMessenger createMsnMessenger() {
        return MsnMessengerFactory.createMsnMessenger(getId(), getPassword());
    }

    private void defaultResponse(MsnSwitchboard switchboard)
    {
        switchboard.sendText(getText("presencereporter.msn.message.switchboard"));
    }

    private String getStatusImage(String status)
    {
        String img = STATUS_MAP.get(status);
        if (img == null)
        {
            log.info("Unrecognised MSN status: " + status);
            img = "im_invisible";
        }
        return img;
    }
}
