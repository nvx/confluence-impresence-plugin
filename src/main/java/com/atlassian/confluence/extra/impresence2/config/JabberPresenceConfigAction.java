/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer. * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided with the distribution. * Neither the
 * name of "Atlassian" nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.confluence.extra.impresence2.reporter.JabberPresenceReporter;
import com.atlassian.xwork.RequireSecurityToken;
import org.apache.commons.lang.StringUtils;

import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.isNumeric;

/**
 * Action to configure the Jabber account settings. Maintains functionality for both login presence and server (domain + port) configs.
 */
public class JabberPresenceConfigAction extends LoginPresenceConfigAction
{
    private String domain;
    private String port;
    
    protected String getServiceKey()
    {
        return JabberPresenceReporter.KEY;
    }

    protected String getServiceName()
    {
        return getText("jabber.config.service.name");
    }

    @Override
    public String doDefault() throws Exception
    {
        JabberPresenceReporter reporter = (JabberPresenceReporter) getReporter();
        if (null != reporter)
        {
            setDomain(reporter.getDomain());
            setPort(String.valueOf(reporter.getPort()));
        }

        return super.doDefault();
    }

    @Override
    @RequireSecurityToken(true)
    public String execute() throws Exception
    {
        JabberPresenceReporter reporter = (JabberPresenceReporter) getReporter();
        if (reporter == null)
        {
            addActionError(getText("error.general.nosuchreporter", new String[] { getServiceName() }));
            return ERROR;
        }
        
        reporter.setDomain(StringUtils.trim(getDomain()));
        try
        {
            reporter.setPort(Integer.parseInt(getPort()));
        }
        catch (NumberFormatException invalidPort)
        {
            reporter.setPort(null);
        }

        return super.execute();
    }
    
    @Override
    public void validate()
    {
        super.validate();
        
        if (isNotBlank(port) && !isNumeric(port))
        {
            addActionError(getText("impresence.jabber.error.invalidport", new String[] { port }));
        }
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }
}