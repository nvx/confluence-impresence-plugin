package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.xml.sax.SAXException;


public class MsnPresenceTestCase extends AbstractPresenceTestCase
{
    protected void setUp() throws Exception {
        super.setUp();
        assertTrue(getBandanaHelper("extra.im.account.msn").delete());
        assertTrue(getBandanaHelper("extra.im.password.msn").delete());
    }

    protected boolean requiresConfiguration()
    {
        return true;
    }

    public void testRequiresConfig()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Requires Configuration");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=msn}"
        );

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("An administrator must configure your server's MSN/Windows Messenger Service before you can use this macro.");
        assertLinkPresentWithText("Configure MSN/Windows Messenger Service");
    }

    public void testConfigurationNotAllowedForNonAdminUsers()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Dummy Account Configuration Accessible By Admins Only");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=msn}"
        );

        assertTrue(pageHelper.create());

        logout();
        login(nonAdminUserName, nonAdminPassword);

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("An administrator must configure your server's MSN/Windows Messenger Service before you can use this macro.");
        assertLinkNotPresentWithText("Configure MSN/Windows Messenger Service");

        logout();
        loginAsAdmin(); /* So tearDown can be called successfully */
    }

    private void configureDummyAccount(final String reporterId, final String reporterPassword)
    {
        setWorkingForm("presenceconfigform");
        setTextField("reporterId", reporterId);
        setTextField("reporterPassword", reporterPassword);
        submit("update");

        /* Assert if the values are correctly set */
        assertTitleEquals("Configure the MSN/Windows Messenger Service - Confluence");
        assertTableEquals("presenceconfigstatetable",
                new String[][] {
                        new String[] { "Current ID:", reporterId },
                        new String[] { "Current Password:", reporterPassword },
                });
        assertEquals(reporterId, getElementAttributByXPath("//input[@name='reporterId']", "value"));
        assertEquals(reporterPassword, getElementAttributByXPath("//input[@name='reporterPassword']", "value"));
    }

    public void testShowPresenceWithId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence With ID");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=msn}"
        );

        assertTrue(pageHelper.create());

        try
        {
        	// Configure MSN/Windows Messenger Service requires escalated privileges
	        gotoPageWithEscalatedPrivileges("/pages/viewpage.action?pageId=" + pageHelper.getId());
	        
	        clickLinkWithText("Configure MSN/Windows Messenger Service");
	        assertTitleEquals("Configure the MSN/Windows Messenger Service - Confluence");
	        assertTableEquals("presenceconfigstatetable",
	                new String[][] {
	                        new String[] { "Current ID:", "None!" },
	                        new String[] { "Current Password:", "None!" },
	                });
	
	        assertEquals("", getElementAttributByXPath("//input[@name='reporterId']", "value"));
	        assertEquals("", getElementAttributByXPath("//input[@name='reporterPassword']", "value"));;
	
	        configureDummyAccount("admin@localhost.localdomain", "admin");
	
	        /* Now, let's see if the page shows the presence of the targeted user */
	        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
	
	        assertTextNotPresent("An administrator must configure your server's MSN/Windows Messenger Service before you can use this macro.");
	        assertTrue(hasPresenceImageInCurrentPage());
	        assertLinkPresentWithText("john.doe@localhost.localdomain");
        }
        finally
        {
        	dropEscalatedPrivileges();
        }
    }

    public void testShowPresenceWithoutId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence Without ID");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=msn|showid=false}"
        );

        assertTrue(pageHelper.create());

        try
        {
        	// Configure dummy account requires escalated privileges
        	gotoPageWithEscalatedPrivileges("/admin/plugins/impresence2/msn/config.action");
        	configureDummyAccount("admin@localhost.localdomain", "admin");

        	/* Now, let's see if the page shows the presence of the targeted user */
        	gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        	assertTextNotPresent("An administrator must configure your server's MSN/Windows Messenger Service before you can use this macro.");
        	assertTrue(hasPresenceImageInCurrentPage());
        	assertLinkNotPresentWithText("john.doe@localhost.localdomain");
        }
        finally
        {
        	 dropEscalatedPrivileges();
        }
    }
}
