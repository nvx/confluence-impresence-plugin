package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.xml.sax.SAXException;

public class SametimePresenceTestCase extends AbstractPresenceTestCase
{
    private String targetUser;

    protected void setUp() throws Exception
    {
        super.setUp();
        assertTrue(getBandanaHelper("extra.im.server.name.sametime").delete());
        targetUser = "john.doe@localhost.localdomain";
    }

    protected boolean requiresConfiguration()
    {
        return true;
    }

    public void testRequiresConfig()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Requires Configuration");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=sametime}"
        );

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("An administrator must configure your server's Lotus Sametime Service before you can use this macro.");
        assertLinkPresentWithText("Configure Lotus Sametime Service");
    }

    public void testConfigurationNotAllowedForNonAdminUsers()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Dummy Account Configuration Accessible By Admins Only");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=sametime}"
        );

        assertTrue(pageHelper.create());

        logout();
        login(nonAdminUserName, nonAdminPassword);

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("An administrator must configure your server's Lotus Sametime Service before you can use this macro.");
        assertLinkNotPresentWithText("Configure Lotus Sametime Service");

        logout();
        loginAsAdmin(); /* So tearDown can be called successfully */
    }

    private void configureSametimeServer(final String serverLocation)
    {
        setWorkingForm("configuresametimeform");
        setTextField("server", serverLocation);
        submit("update");

        /* Assert if the values are correctly set */
        assertTitleEquals("Configure the Lotus Sametime Service - Confluence");
        assertTableEquals("sametimeconfigstatetable",
                new String[][] {
                        new String[] { "Current Server:", "http:// " + serverLocation },
                });


        assertEquals(serverLocation, getElementAttributByXPath("//input[@name='server']", "value"));
    }

    public void testShowPresenceWithId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence With ID");
        pageHelper.setContent(
                "{im:" + targetUser + "|service=sametime}"
        );

        assertTrue(pageHelper.create());

        try
        {
        	// Configure Lotus Sametime Service requires escalated privileges
        	gotoPageWithEscalatedPrivileges("/pages/viewpage.action?pageId=" + pageHelper.getId());

        	clickLinkWithText("Configure Lotus Sametime Service");

        	assertTitleEquals("Configure the Lotus Sametime Service - Confluence");
        	assertTableEquals("sametimeconfigstatetable",
        			new String[][] {
        			new String[] { "Current Server:", "None!" },
        	});
        	assertEquals("", getElementAttributByXPath("//input[@name='server']", "value"));

        	configureSametimeServer("localhost.localdomain");

        	/* Now, let's see if the page shows the presence of the targeted user */
        	gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        	assertTextNotPresent("An administrator must configure your server's Lotus Sametime Service before you can use this macro.");
        	assertElementPresentByXPath("//div[@class='wiki-content']//script[text()='" + "writeSametimeLink(\"" + targetUser + "\", \"" + targetUser + "\", true)" + "']");
        }
        finally
        {
        	 dropEscalatedPrivileges();
        }
    }
}
