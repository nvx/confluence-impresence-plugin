package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import org.xml.sax.SAXException;

import java.util.Arrays;

public abstract class AbstractPresenceTestCase extends AbstractConfluencePluginWebTestCase
{

    protected String testSpaceKey;

    protected String nonAdminUserName;

    protected String nonAdminPassword;

    protected void setUp() throws Exception
    {
        super.setUp();

        if (requiresConfiguration())
        {
            /*
             * We create a non admin user which can be used to test for the ability to configure dummy accounts.
             * Non admin users should not be able to configure the plugin.
             */
            createNonAdminUser();
        }

        testSpaceKey = "tst";
    }

    private void createNonAdminUser()
    {
        final UserHelper userHelper = getUserHelper();

        userHelper.setName(nonAdminUserName = "john.doe");
        userHelper.setFullName("John Doe");
        userHelper.setEmailAddress(userHelper.getName() + "@localhost.localdomain");
        userHelper.setGroups(Arrays.asList("confluence-users" ));
        userHelper.setPassword(nonAdminPassword = "john.doe");

        assertTrue(userHelper.create());
    }

    protected boolean hasPresenceImageInCurrentPage() throws SAXException
    {
        final String sourcePrefix = getContextPath() + "/download/resources/confluence.extra.impresence2:im/images/";
        return getDialog().hasElementByXPath("//img[starts-with(@src,'" + sourcePrefix + "')]");
    }

    private String getContextPath()
    {
        return getElementAttributByXPath("//meta[@id='confluence-context-path']", "content");
    }

    protected abstract boolean requiresConfiguration();
}
