package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.confluence.extra.impresence2.reporter.GtalkPresenceReporter;

public class GtalkPresenceConfigActionTestCase extends LoginPresenceConfigActionTestCase<GtalkPresenceConfigAction, GtalkPresenceReporter>
{
    protected GtalkPresenceConfigAction createAction()
    {
        GtalkPresenceConfigAction action = new GtalkPresenceConfigAction()
        {
            public String getText(String key, Object[] substitute)
            {
                return key;
            }

            protected String getServiceName()
            {
                return "Google Talk";
            }
        };
        
        action.setPresenceManager(presenceManager);
        return action;
    }

    protected String getServiceKey()
    {
        return "gtalk";
    }

    protected Class<GtalkPresenceReporter> getPresenceReporterClass()
    {
        return GtalkPresenceReporter.class;
    }
}
