package com.atlassian.confluence.extra.impresence2.reporter;

import java.io.IOException;

public class TestSkypePresenceReporter extends AbstractPresenceReporterTest<SkypePresenceReporter>
{
    protected String getPresenceReporterKey()
    {
        return SkypePresenceReporter.KEY;
    }

    protected SkypePresenceReporter createPresenceReporter()
    {
        SkypePresenceReporter presenceReporter = new SkypePresenceReporter();
        presenceReporter.setLocaleSupport(localeSupport);
        return presenceReporter;
    }

    public void testGetKey()
    {
        assertEquals("skype", createPresenceReporter().getKey());
    }

    public void testGetName()
    {
        assertEquals("presencereporter.skype.name", createPresenceReporter().getName());
    }

    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.skype.servicehomepage", createPresenceReporter().getServiceHomepage());
    }

    public void testHasConfig()
    {
        assertEquals(false, createPresenceReporter().hasConfig());
    }

    public void testRequiresConfig()
    {
        assertEquals(false, createPresenceReporter().requiresConfig());
    }

    public void testGetPresenceXHTMLWhenIdIsNotSpecified() throws IOException, PresenceException
    {
        assertTrue(createPresenceReporter().getPresenceXHTML(null, false).indexOf("presencereporter.skype.error.noscreenname") >= 0);
    }

    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";
        final StringBuffer out = new StringBuffer();

        out.append("<script type='text/javascript' src='http://download.skype.com/share/skypebuttons/js/skypeCheck.js'></script>")
                .append("<a href='skype:").append(id).append("?call'>")
                .append("<img src='http://mystatus.skype.com/smallclassic/").append(id).append("' style='border: none;' align='absmiddle' alt='My status' />")
                .append("</a>");

        out.append("&nbsp;<a href='skype:").append(id).append("?call'>")
                .append(id)
                .append("</a>");

        assertEquals(
                out.toString(),
                createPresenceReporter().getPresenceXHTML(id, true));
    }

    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";
        final StringBuffer out = new StringBuffer();


        out.append("<script type='text/javascript' src='http://download.skype.com/share/skypebuttons/js/skypeCheck.js'></script>")
                .append("<a href='skype:").append(id).append("?call'>")
                .append("<img src='http://mystatus.skype.com/smallclassic/").append(id).append("' style='border: none;' align='absmiddle' alt='My status' />")
                .append("</a>");

        assertEquals(
                out.toString(),
                createPresenceReporter().getPresenceXHTML(id, false));
    }
}