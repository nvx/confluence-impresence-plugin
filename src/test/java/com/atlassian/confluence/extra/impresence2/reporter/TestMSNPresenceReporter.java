package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import net.sf.jml.Email;
import net.sf.jml.MsnContact;
import net.sf.jml.MsnContactList;
import net.sf.jml.MsnMessenger;
import net.sf.jml.MsnUserStatus;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestMSNPresenceReporter extends AbstractLoginPresenceReporterTest<MSNPresenceReporter>
{
    @Mock
    private MsnMessenger msnMessenger;

    @Mock
    private MsnContactList msnContactList;

    @Mock
    private MsnContact msnContact;

    private boolean loginComplete;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        when(bootstrapManager.getWebAppContextPath()).thenReturn("");
    }

    protected String getPresenceReporterKey()
    {
        return MSNPresenceReporter.KEY;
    }

    protected MSNPresenceReporter createPresenceReporter()
    {
        MSNPresenceReporter presenceReporter = new MSNPresenceReporter()
        {
            protected MsnMessenger createMsnMessenger()
            {
                return msnMessenger;
            }

            protected boolean isLoginComplete()
            {
                return loginComplete;
            }
        };
        presenceReporter.setLocaleSupport(localeSupport);
        presenceReporter.setBootstrapManager(bootstrapManager);
        presenceReporter.setBandanaManager(bandanaManager);
        return presenceReporter;
    }

    public void testGetKey()
    {
        assertEquals("msn", createPresenceReporter().getKey());
    }

    public void testGetName()
    {
        assertEquals("presencereporter.msn.name", createPresenceReporter().getName());
    }

    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.msn.servicehomepage", createPresenceReporter().getServiceHomepage());
    }

    public void testHasConfig()
    {
        assertEquals(true, createPresenceReporter().hasConfig());
    }

    public void testGetPresenceXHtmlWhenPresenceRequiresConfig()
    {
        createPresenceReporter();
        presenceReporter.setLocaleSupport(localeSupport);

        try
        {
            presenceReporter.getPresenceXHTML("foo", true);
            fail("MsnPresenceReporter should throw a PresenceException if a dummy account has not been configured.");
        }
        catch (final PresenceException pe)
        {
            assertEquals("presencereporter.msn.error.needconfig", pe.getMessage());
        }
    }

    public void testGetPresenceXhtmlWhenDummyAccountIsStillLoggingIn() throws PresenceException
    {

        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "extra.im.account." + getPresenceReporterKey()
        )).thenReturn("username");
        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "extra.im.password." + getPresenceReporterKey()
        )).thenReturn("password");


        createPresenceReporter();
        assertTrue(presenceReporter.getPresenceXHTML("admin@localhost.localdomain", true).indexOf("presence.link.loggingin") >= 0);
    }

    public void testGetPresenceXhtmlWhenTargetIdIsNotFriend() throws PresenceException
    {
        final String emailAddress = "admin@localhost.localdomain";
        final Email email = Email.parseStr(emailAddress);

        loginComplete = true;

        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "extra.im.account." + getPresenceReporterKey()
        )).thenReturn("username");
        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "extra.im.password." + getPresenceReporterKey()
        )).thenReturn("password");

        when(msnMessenger.getContactList()).thenReturn(msnContactList);


        createPresenceReporter();
        assertTrue(presenceReporter.getPresenceXHTML("admin@localhost.localdomain", true).indexOf("presencereporter.msn.message.waitinbuddyaccept") >= 0);
        verify(msnMessenger).addFriend(email, emailAddress);
    }

    public void testGetPresenceXhtmlWhenFriendIsAvailableShowingId() throws PresenceException
    {
        final String emailAddress = "admin@localhost.localdomain";
        final Email email = Email.parseStr(emailAddress);
        final StringBuffer out = new StringBuffer();
        final String url;

        loginComplete = true;

        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "extra.im.account." + getPresenceReporterKey()
        )).thenReturn("username");
        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "extra.im.password." + getPresenceReporterKey()
        )).thenReturn("password");

        when(msnContact.getStatus()).thenReturn(MsnUserStatus.ONLINE);
        when(msnContactList.getContactByEmail(email)).thenReturn(msnContact);
        when(msnMessenger.getContactList()).thenReturn(msnContactList);

        url = out.append("mailto:").append(emailAddress).toString();

        out.setLength(0);
        out.append("<A href='").append(url).append("'>");

        out.append("<img src='").append(StringUtils.EMPTY)
                .append("/download/resources/confluence.extra.impresence2:im/images/").append("im_available").append(".gif'")
                .append(" style='vertical-align:bottom; margin:0px 1px;' border='0'")
                .append("' title='").append(MsnUserStatus.ONLINE.getDisplayStatus()).append("'")
                .append("/>");

        out.append("</a>");
        out.append("&nbsp;");
        out.append("<A href='").append(url).append("' title='").append(MsnUserStatus.ONLINE.getDisplayStatus()).append("'>");
        out.append(emailAddress);
        out.append("</a>");

        createPresenceReporter();
        assertEquals(out.toString(), presenceReporter.getPresenceXHTML("admin@localhost.localdomain", true));
    }

    public void testGetPresenceXhtmlWhenFriendIsAvailableNotShowingId() throws PresenceException
    {
        final String emailAddress = "admin@localhost.localdomain";
        final Email email = Email.parseStr(emailAddress);
        final StringBuffer out = new StringBuffer();
        final String url;

        loginComplete = true;

        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "extra.im.account." + getPresenceReporterKey()
        )).thenReturn("username");
        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                "extra.im.password." + getPresenceReporterKey()
        )).thenReturn("password");

        when(msnContact.getStatus()).thenReturn(MsnUserStatus.ONLINE);
        when(msnContactList.getContactByEmail(email)).thenReturn(msnContact);
        when(msnMessenger.getContactList()).thenReturn(msnContactList);

        url = out.append("mailto:").append(emailAddress).toString();

        out.setLength(0);
        out.append("<A href='").append(url).append("'>");

        out.append("<img src='").append(StringUtils.EMPTY)
                .append("/download/resources/confluence.extra.impresence2:im/images/").append("im_available").append(".gif'")
                .append(" style='vertical-align:bottom; margin:0px 1px;' border='0'")
                .append("' title='").append(MsnUserStatus.ONLINE.getDisplayStatus()).append("'")
                .append("/>");
        out.append("</a>");


        createPresenceReporter();
        assertEquals(out.toString(), presenceReporter.getPresenceXHTML("admin@localhost.localdomain", false));
    }
}