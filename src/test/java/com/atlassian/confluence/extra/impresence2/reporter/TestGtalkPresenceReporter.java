package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.apache.commons.lang.StringUtils;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.RosterPacket;
import org.mockito.Mock;

import java.io.IOException;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGtalkPresenceReporter extends AbstractLoginPresenceReporterTest<GtalkPresenceReporter>
{
    @Mock
    private XMPPConnection xmppConnection;

    @Mock
    private Roster roster;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, ID_PREFIX + getPresenceReporterKey())).thenReturn("john.doe");
        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, PASSWORD_PREFIX + getPresenceReporterKey())).thenReturn("secret");

        when(xmppConnection.isConnected()).thenReturn(true);
        when(xmppConnection.isAuthenticated()).thenReturn(true);
        when(xmppConnection.getRoster()).thenReturn(roster);
    }

    protected GtalkPresenceReporter createPresenceReporter()
    {
        GtalkPresenceReporter presenceReporter = new GtalkPresenceReporter()
        {
            @Override
            XMPPConnection createXmppConnection(String domainName)
            {
                return xmppConnection;
            }
        };
        presenceReporter.setLocaleSupport(localeSupport);
        presenceReporter.setBootstrapManager(bootstrapManager);
        presenceReporter.setBandanaManager(bandanaManager);
        return presenceReporter;
    }

    protected String getPresenceReporterKey()
    {
        return GtalkPresenceReporter.KEY;
    }

    public void testGetKey()
    {
        assertEquals("gtalk", presenceReporter.getKey());
    }

    public void testGetName()
    {
        assertEquals("presencereporter.gtalk.name", presenceReporter.getName());
    }

    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.gtalk.servicehomepage", presenceReporter.getServiceHomepage());
    }

    public void testHasConfig()
    {
        assertTrue(createPresenceReporter().hasConfig());
    }

    public void testGetPresenceXhtmlWhenAddingBuddy() throws IOException, PresenceException, XMPPException
    {
        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("presencereporter.gtalk.message.waitinbuddyaccept") >= 0);
    }

    public void testGetPresenceXhtmlWhenWaitingForBuddyUnblock() throws IOException, PresenceException, XMPPException
    {
        final RosterEntry aRosterEntry = mock(RosterEntry.class);

        when(roster.contains(anyString())).thenReturn(true);
        when(roster.getEntry(anyString())).thenReturn(aRosterEntry);

        when(aRosterEntry.getName()).thenReturn("jane.doe");
        when(aRosterEntry.getUser()).thenReturn("jane.doe");
        when(aRosterEntry.getStatus()).thenReturn(RosterPacket.ItemStatus.SUBSCRIPTION_PENDING);

        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("presence.link.waitingunblock") >= 0);
    }

    public void testGetPresenceXhtmlWhenPresenceCouldNotBeObtained() throws IOException, PresenceException, XMPPException
    {
        final RosterEntry aRosterEntry = mock(RosterEntry.class);

        when(roster.contains(anyString())).thenReturn(true);
        when(roster.getEntry(anyString())).thenReturn(aRosterEntry);

        when(aRosterEntry.getName()).thenReturn("jane.doe");
        when(aRosterEntry.getUser()).thenReturn("jane.doe");

        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("presence.link.intermediate") >= 0);
    }

    public void testGetPresenceXhtmlWhenPresenceCouldBeObtained() throws IOException, PresenceException, XMPPException
    {
        final RosterEntry aRosterEntry = mock(RosterEntry.class);

        when(roster.contains(anyString())).thenReturn(true);
        when(roster.getEntry(anyString())).thenReturn(aRosterEntry);

        when(aRosterEntry.getName()).thenReturn("jane.doe");
        when(aRosterEntry.getUser()).thenReturn("jane.doe");

        when(roster.getPresence(anyString())).thenReturn(
                new Presence(Presence.Type.available)
                {
                    @Override
                    public Mode getMode()
                    {
                        return Mode.away;
                    }
                }
        );

        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("away") >= 0);
    }

    public void testGetPresenceXhtmlWhenPresenceModeIsNullAndPresenceTypeIsAvailable() throws IOException, PresenceException, XMPPException
    {
        final RosterEntry aRosterEntry = mock(RosterEntry.class);

        when(roster.contains(anyString())).thenReturn(true);
        when(roster.getEntry(anyString())).thenReturn(aRosterEntry);

        when(aRosterEntry.getName()).thenReturn("jane.doe");
        when(aRosterEntry.getUser()).thenReturn("jane.doe");

        when(roster.getPresence(anyString())).thenReturn(
                new Presence(Presence.Type.available)
                {
                    @Override
                    public Mode getMode()
                    {
                        return null;
                    }
                }
        );

        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("im_free_chat.gif") >= 0);
    }

    public void testXmppConnectionCreatedWithTheDomainOfTheConfiguredDummyUser() throws IOException, PresenceException
    {
        final String customDomain = "foo.bar.baz";

        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, ID_PREFIX + getPresenceReporterKey())).thenReturn("john.doe@" + customDomain);

        presenceReporter = new GtalkPresenceReporter()
        {
            @Override
            XMPPConnection createXmppConnection(String domainName)
            {
                if (!StringUtils.equals(customDomain, domainName))
                    fail("XMPP connection not created with domain " + customDomain);
                return xmppConnection;
            }
        };
        presenceReporter.setLocaleSupport(localeSupport);
        presenceReporter.setBootstrapManager(bootstrapManager);
        presenceReporter.setBandanaManager(bandanaManager);

        assertNotNull(presenceReporter.getPresenceXHTML("jane.doe", true));
    }

    public void testGetPresenceXhtmlWhenNotAuthenticatedToGtalk() throws IOException, PresenceException, XMPPException
    {
        when(xmppConnection.isAuthenticated()).thenReturn(false);

        assertTrue(presenceReporter.getPresenceXHTML("jane.doe", true).indexOf("presencereporter.gtalk.error.login") >= 0);
    }
}