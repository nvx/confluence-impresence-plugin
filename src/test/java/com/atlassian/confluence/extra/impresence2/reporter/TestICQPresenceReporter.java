package com.atlassian.confluence.extra.impresence2.reporter;

import java.io.IOException;

public class TestICQPresenceReporter extends AbstractPresenceReporterTest<ICQPresenceReporter>
{
    protected String getPresenceReporterKey()
    {
        return ICQPresenceReporter.KEY;
    }

    protected ICQPresenceReporter createPresenceReporter()
    {
        ICQPresenceReporter presenceReporter = new ICQPresenceReporter();
        presenceReporter.setLocaleSupport(localeSupport);
        return presenceReporter;
    }

    public void testGetKey()
    {
        assertEquals("icq", ICQPresenceReporter.KEY);
    }

    public void testGetName()
    {
        assertEquals("presencereporter.icq.name", presenceReporter.getName());
    }

    public void testGetServiceHomePage()
    {
        assertEquals("presencereporter.icq.servicehomepage", presenceReporter.getServiceHomepage());
    }

    public void testHasConfig()
    {
        assertEquals(false, presenceReporter.hasConfig());
    }

    public void testRequiresConfig()
    {
        assertEquals(false, presenceReporter.requiresConfig());
    }

    public void testGetPresenceXHTMLWhenIdIsNotSpecified() throws IOException, PresenceException
    {
        assertTrue(presenceReporter.getPresenceXHTML(null, false).indexOf("presencereporter.icq.error.nouin") >= 0);
    }

    public void testGetPresenceXHTMLWithIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";

        assertEquals(
                "<a title=\"ICQ " + id + "\" href=\"http://wwp.icq.com/scripts/contact.dll?msgto="
                        + id
                        + "\"><img border=\"0\" align=\"absmiddle\""
                        + " src=\"http://status.icq.com/online.gif?icq="
                        + id
                        + "&img=5\"></a>"
                        + "&nbsp<a title=\"ICQ "
                        + id
                        + "\" href=\"http://wwp.icq.com/scripts/contact.dll?msgto="
                        + id
                        + "\">"
                        + id
                        + "</a>",
                presenceReporter.getPresenceXHTML(id, true));
    }

    public void testGetPresenceXHTMLWithoutIdOutput() throws IOException, PresenceException
    {
        final String id = "foo";

        assertEquals(
                "<a title=\"ICQ " + id + "\" href=\"http://wwp.icq.com/scripts/contact.dll?msgto="
                        + id
                        + "\"><img border=\"0\" align=\"absmiddle\" "
                        + "src=\"http://status.icq.com/online.gif?icq="
                        + id
                        + "&img=5\"></a>",
                presenceReporter.getPresenceXHTML(id, false));
    }
}